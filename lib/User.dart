class User {
  String _name;
  String _surname;
  int _age;

  User(this._name, this._surname, this._age);

  static User fromJson(dynamic json) {
    return User(
        json['name'] as String, json['surname'] as String, json['age'] as int);
  }

  @override
  String toString() {
    return "User => \nname =  $_name, \nsurname = $_surname, \nage = $_age";
  }
}
