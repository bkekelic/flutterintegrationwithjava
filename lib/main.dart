import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'User.dart';

void main() {
  return runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static const platform = const MethodChannel("flutter.native/helper");
  static const eventChargingChannel =
      const EventChannel("flutter.native/helper/eventCharging");
  static const eventAsyncChannel =
      const EventChannel("flutter.native/helper/eventAsync");

  String _nativeResult = "click button";

  Future<void> _responseFromNativeCode() async {
    String _response = "";
    try {
      _response = await platform.invokeMethod("myJavaFunction");
    } on PlatformException catch (e) {
      _response = e.message;
    }
    setState(() {
      _nativeResult = _response;
    });
  }

  Future<void> _squareTheNumber(int number) async {
    int _response;
    try {
      _response =
          await platform.invokeMethod("squareTheNumber", {"num": number});
    } on PlatformException catch (e) {
      _response = 0;
    }
    setState(() {
      _nativeResult = _response.toString();
    });
  }

  Future<void> _sumTheNumbers(int num1, int num2) async {
    int _response;
    try {
      _response = await platform.invokeMethod(
          "sumTwoNumbers", <String, dynamic>{"num1": num1, "num2": num2});
    } on PlatformException catch (e) {
      _response = 0;
    }
    setState(() {
      _nativeResult = _response.toString();
    });
  }

  /// Receives object from Java code
  ///
  Future<void> _getObjectFromJavaCode() async {
    User _user = User("1", "2", 3);
    try {
      final _response = await platform.invokeMethod("getUserObject");
      _user = User.fromJson(json.decode(_response));
    } on PlatformException catch (e) {
      _user = User("", "", 0);
    }
    setState(() {
      _nativeResult = _user.toString();
    });
  }

  /// Methods for getting battery level and battery charging status
  ///
  String _batteryLevel = "";
  String _chargingStatus = "";

  Future<void> _getBatteryLevel() async {
    /// This is for the battery status
    eventChargingChannel
        .receiveBroadcastStream()
        .listen(_onEventCharging, onError: _onErrorCharging);

    String batteryLevel;
    try {
      final int result = await platform.invokeMethod('getBatteryLevel');
      batteryLevel = 'Battery level: $result%.';
    } on PlatformException {
      batteryLevel = 'Failed to get battery level.';
    }
    setState(() {
      _batteryLevel = batteryLevel;
    });
  }

  void _onEventCharging(event) {
    setState(() {
      _chargingStatus =
          "Battery status: ${event == 'charging' ? '' : 'dis'}charging.";
    });
  }

  void _onErrorCharging(event) {
    setState(() {
      _chargingStatus = 'Battery status: unknown.';
    });
  }

  /// Starts async task in Java code
  ///
  Future<void> _doSomeAsyncTaskInJava() async {
    eventAsyncChannel
        .receiveBroadcastStream()
        .listen(_onEventAsyncTask, onError: _onErrorAsyncTask);
  }

  void _onEventAsyncTask(event) {
    setState(() {
      _nativeResult = event.toString();
    });
  }

  void _onErrorAsyncTask(event) {
    setState(() {
      _nativeResult = event.toString();
    });
  }
bool _checkValue = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text("Keep pressing to see if ui is responsive "),
                Checkbox(
                  value: _checkValue,
                  onChanged: (value){
                    setState(() {
                      _checkValue = value;
                    });
                  },
                ),
              ],
            ),
            SizedBox(height: 50,),
            Text(
              '$_nativeResult',
            ),
            Text(
              '$_batteryLevel',
            ),
            Text(
              '$_chargingStatus',
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          /// Put you method invocation here
          ///
          _doSomeAsyncTaskInJava();
        },
        child: Icon(Icons.call),
      ),
    );
  }
}
