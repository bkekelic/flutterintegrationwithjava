package net.atos.integrattionflutterandjava;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.CountDownTimer;
import android.util.Log;

import androidx.annotation.NonNull;

import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {

    private static final String CHANNEL = "flutter.native/helper";
    private static final String CHARGING_CHANNEL = "flutter.native/helper/eventCharging";
    private static final String ASYNC_CHANNEL = "flutter.native/helper/eventAsync";

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);

        // Event channel for battery status
        //
        new EventChannel(flutterEngine.getDartExecutor(), CHARGING_CHANNEL).setStreamHandler(
                new EventChannel.StreamHandler() {

                    private BroadcastReceiver chargingStateChangeReceiver;

                    @Override
                    public void onListen(Object arguments, EventChannel.EventSink events) {
                        chargingStateChangeReceiver = createChargingStateChangeReceiver(events);
                        registerReceiver(chargingStateChangeReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
                    }

                    @Override
                    public void onCancel(Object arguments) {
                        unregisterReceiver(chargingStateChangeReceiver);
                        chargingStateChangeReceiver = null;
                    }
                }
        );

        // Event channel for async task
        //
        new EventChannel(flutterEngine.getDartExecutor(), ASYNC_CHANNEL).setStreamHandler(
                new EventChannel.StreamHandler() {
                    @Override
                    public void onListen(Object arguments, EventChannel.EventSink events) {

                        // TODO uncomment for async task
//                        DoSomeBackgroundWork worker = new DoSomeBackgroundWork();
//                        try {
//                            String tempString = "Hello";
//                            String fullString = worker.execute(tempString).get();
//                            events.success(fullString);
//                        } catch (ExecutionException e) {
//                            events.error(e.getLocalizedMessage(), e.getMessage(), e);
//                        } catch (InterruptedException e) {
//                            events.error(e.getLocalizedMessage(), e.getMessage(), e);
//                        }


                        // Faking async task just to make ui responsive

                        CountDownTimer timer;
                        timer = new CountDownTimer(5000, 3) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                                events.success("Countdown " + millisUntilFinished);
                            }

                            @Override
                            public void onFinish() {
                                try {
                                    events.success("Finished after 5 sec");
                                } catch (Exception e) {
                                    Log.e("Error", "Error: " + e.toString());
                                }
                            }
                        }.start();

                    }

                    @Override
                    public void onCancel(Object arguments) {

                    }
                }
        );


        new MethodChannel(flutterEngine.getDartExecutor().getBinaryMessenger(), CHANNEL).setMethodCallHandler(
                new MethodChannel.MethodCallHandler() {
                    @Override
                    public void onMethodCall(MethodCall call, MethodChannel.Result result) {
                        if (call.method.equals("myJavaFunction")) {
                            String response = myJavaFunction();
                            result.success(response);
                        } else if (call.method.equals("squareTheNumber")) {
                            int number = call.argument("num");
                            int squaredNumber = squareTheNumber(number);
                            result.success(squaredNumber);
                        } else if (call.method.equals("sumTwoNumbers")) {
                            int num1 = call.argument("num1");
                            int num2 = call.argument("num2");
                            int summedNumber = sumTwoNumbers(num1, num2);
                            result.success(summedNumber);
                        } else if (call.method.equals("getUserObject")) {
                            User user = new User("Name1", "Surname1", 23);
                            String jsonResponse = user.toJson();
                            result.success(jsonResponse);
                        } else if (call.method.equals("getBatteryLevel")) {
                            int batteryLevel = getBatteryLevel();
                            if (batteryLevel != -1) {
                                result.success(batteryLevel);
                            } else {
                                result.error("UNAVAILABLE", "Battery level not available.", null);
                            }
                        } else {
                            result.notImplemented();
                        }
                    }
                }
        );
    }

    private BroadcastReceiver createChargingStateChangeReceiver(EventChannel.EventSink events) {
        return new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                int status = intent.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
                if (status == BatteryManager.BATTERY_STATUS_UNKNOWN) {
                    events.error("UNAVAILABLE", "Charging status unavailable", null);
                } else {
                    boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                            status == BatteryManager.BATTERY_STATUS_FULL;
                    events.success(isCharging ? "charging" : "discharging");
                }
            }
        };
    }

    private int getBatteryLevel() {
        BatteryManager batteryManager = (BatteryManager) getSystemService(BATTERY_SERVICE);
        return batteryManager.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
    }


    private String myJavaFunction() {
        return "Hello from Java code";
    }

    private Integer squareTheNumber(Integer num) {
        return num * num;
    }

    private Integer sumTwoNumbers(int num1, int num2) {
        return num1 + num2;
    }

}
