package net.atos.integrattionflutterandjava;

import android.os.AsyncTask;

public class DoSomeBackgroundWork extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... strings) {
        String temp = strings[0];
        temp += " after 2 seconds";
        try {
            Thread.sleep(2000);
            return temp;
        } catch (InterruptedException e) {
            return e.getMessage();
        }
    }
}
