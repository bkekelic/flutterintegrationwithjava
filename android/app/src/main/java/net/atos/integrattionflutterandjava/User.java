package net.atos.integrattionflutterandjava;

import com.google.gson.Gson;

public class User {
    private String name;
    private String surname;
    private int age;

    public User(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public String toJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }
}
